var mongoose = require("mongoose");
var Bicicleta = require("../../models/bicicleta");

describe("Testing Bicicletas", function () {
	beforeEach(function (done) {
		var mongoDB = "mongodb://localhost/testdb";
		mongoose.connect(mongoDB, {
			useNewUrlParser: true,
			useUnifiedTopology: true,
		});
		mongoose.set("useCreateIndex", true); //Para las versiones modernas
		const db = mongoose.connection;
		db.on("error", console.error.bind(console, "connection error"));
		db.once("open", function () {
			console.log("We are connected to test database!");
			done();
		});
	});

	afterEach(function (done) {
		Bicicleta.deleteMany({}, function (err, success) {
			if (err) {
				console.log(err);
			}
			mongoose.disconnect(err);
			done();
		});
	});

	describe("Bicicleta.createInstance", () => {
		it("Crear una instancia de Bicicleta", () => {
			var bici = Bicicleta.createInstance(1, "azul", "urbana", [
				-40,
				-40,
			]);
			expect(bici.code).toEqual(1);
			expect(bici.color).toBe("azul");
			expect(bici.modelo).toBe("urbana");
			expect(bici.ubicacion[0]).toEqual(-40);
			expect(bici.ubicacion[1]).toEqual(-40);
			console.log("Bici creada: " + bici);
		});
	});
	describe("Bicicleta.allBicis", () => {
		it("Comienza vacia", (done) => {
			Bicicleta.allBicis(function (err, bicis) {
				expect(bicis.length).toBe(0);
				done();
				console.log("Bicis Vacias");
			});
		});
	});
	describe("Bicicleta.add", () => {
		it("Agrega solo una bici", (done) => {
			var aBici = new Bicicleta({
				code: 1,
				color: "azul",
				modelo: "urbana",
			});
			Bicicleta.add(aBici, function (err, newBici) {
				if (err) console.log(err);
				Bicicleta.allBicis(function (err, bicis) {
					expect(bicis.length).toEqual(1);
					expect(bicis[0].code).toEqual(aBici.code);
					done();
					console.log("Bici creada correctamente");
				});
			});
		});
	});
	describe("Bicicleta.findByCode", () => {
		it("Debe devolver la bici con code 1", (done) => {
			Bicicleta.allBicis(function (err, bicis) {
				expect(bicis.length).toBe(0);
				var aBici = new Bicicleta({
					code: 1,
					color: "verde",
					modelo: "urbana",
				});
				Bicicleta.add(aBici, function (err, newBici) {
					if (err) console.log(err);
					var aBici2 = new Bicicleta({
						code: 2,
						color: "roja",
						modelo: "urbana",
					});
					Bicicleta.add(aBici2, function (err, newBici) {
						if (err) console.log(err);
						Bicicleta.findByCode(1, function (err, targetBici) {
							expect(targetBici.code).toBe(aBici.code);
							expect(targetBici.color).toBe(aBici.color);
							expect(targetBici.modelo).toBe(aBici.modelo);
							done();
							console.log(
								"Bici encontrada por codigo correctamente"
							);
						});
					});
				});
			});
		});
	});
	describe("Bicicleta.removeByCode", () => {
		it("Debe Eliminar la bici con el code 1", (done) => {
			Bicicleta.allBicis(function (err, bicis) {
				expect(bicis.length).toBe(0);
				var aBici = new Bicicleta({
					code: 1,
					color: "verde",
					modelo: "urbana",
				});
				Bicicleta.add(aBici, function (err, newBici) {
					if (err) console.log(err);
					var aBici2 = new Bicicleta({
						code: 2,
						color: "roja",
						modelo: "urbana",
					});
					Bicicleta.add(aBici2, function (err, newBici) {
						if (err) console.log(err);
						Bicicleta.removeByCode(1, function (err, targetBici) {
							if (err) console.log(err);
							Bicicleta.allBicis(function (err, bicis) {
								expect(bicis.length).toBe(1);
								done();
								console.log(
									"Bici con code 1 eliminada correctamente"
								);
							});
						});
					});
				});
			});
		});
	});
});
