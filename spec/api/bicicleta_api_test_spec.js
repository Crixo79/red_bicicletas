var mongoose = require("mongoose");
var Bicicleta = require("../../models/bicicleta");
var request = require("request");
var server = require("../../bin/www");
var base_url = "http://localhost:3000/api/bicicletas";
describe("Bicicleta API: ", () => {
    beforeAll(function (done) {
        mongoose.connection.close().then(() => {
            mongoose.connect("mongodb://localhost/testdb", {
                useUnifiedTopology: true,
                useFindAndModify: false,
                useCreateIndex: true,
                useNewUrlParser: true,
            });
            var db = mongoose.connection;
            db.on("error", console.error.bind(console, "connection error"));
            db.once("open", function () {
                console.log("We are connected to test database!");
                done();
            });
        });
    });

    afterEach(function (done) {
        Bicicleta.deleteMany({}, function (err, success) {
            if (err) console.log(err);
            done();
        });
    });

    describe("GET BICICLETAS /", () => {
        it("Status 200", (done) => {
            request.get(base_url, function (error, response, body) {
                var result = JSON.parse(body);
                expect(response.statusCode).toBe(200);
                Bicicleta.allBicis(function (err, bicis) {
                    expect(bicis.length).toBe(0);
                    done();
                    console.log("Listar las bicis en la api correctamente");
                });
            });
        });
    });
    describe("POST BICICLETAS /create", () => {
        it("Status 200", (done) => {
            var headers = { "Content-Type": "application/json" };
            var aBici =
                '{ "code": 10, "color": "verde", "modelo": "urbana", "lat": -40, "lng": -40 }';
            request.post(
                {
                    headers: headers,
                    url: base_url + "/create",
                    body: aBici,
                },
                function (error, response, body) {
                    expect(response.statusCode).toBe(200);
                    var result = JSON.parse(body);
                    console.log(result);
                    expect(result.bicicleta.color).toBe("verde");
                    expect(result.bicicleta.modelo).toBe("urbana");
                    expect(result.bicicleta.ubicacion[0]).toBe(-40);
                    expect(result.bicicleta.ubicacion[1]).toBe(-40);
                    done();
                    console.log("Crear una bici en la api correctamente");
                }
            );
        });
    });

    describe("DELETE BICICLETAS /delete", () => {
        it("Status 204", (done) => {
            var aBici =
                '{ "code": 11, "color": "azul", "modelo": "urbana", "lat": -40, "lng": -40 }';
            var headers = { "Content-Type": "application/json" };
            request.post(
                {
                    headers: headers,
                    url: base_url + "/create",
                    body: aBici,
                },
                function (error, response, body) {
                    expect(response.statusCode).toBe(200);
                    var result = JSON.parse(body);
                    console.log(result);
                    expect(result.bicicleta.color).toBe("azul");
                    expect(result.bicicleta.modelo).toBe("urbana");
                    expect(result.bicicleta.ubicacion[0]).toBe(-40);
                    expect(result.bicicleta.ubicacion[1]).toBe(-40);
                    console.log(
                        "Crear una  para eliminarla bici en la api correctamente"
                    );
                    var aBici2 = '{ "code": 11 }';
                    request.delete(
                        {
                            headers: headers,
                            url: base_url + "/delete",
                            body: aBici2,
                        },
                        function (error, response, body) {
                            expect(response.statusCode).toBe(204);
                            Bicicleta.allBicis(function (err, bicis) {
                                expect(bicis.length).toBe(0);
                                done();
                                console.log(
                                    "Eliminado desde la api correctamente"
                                );
                            });
                        }
                    );
                }
            );
        });
    });
    describe("UPDATE (POST) Bicicletas /update", () => {
        it("Status 200", (done) => {
            var aBici =
                '{ "code": 10, "color": "verde", "modelo": "urbana", "lat": -40, "lng": -40 }';
            var headers = { "Content-Type": "application/json" };
            request.post(
                {
                    headers: headers,
                    url: base_url + "/create",
                    body: aBici,
                },
                function (error, response, body) {
                    expect(response.statusCode).toBe(200);
                    var result = JSON.parse(body);
                    console.log(result);
                    expect(result.bicicleta.color).toBe("verde");
                    expect(result.bicicleta.modelo).toBe("urbana");
                    expect(result.bicicleta.ubicacion[0]).toBe(-40);
                    expect(result.bicicleta.ubicacion[1]).toBe(-40);
                    console.log(
                        "Crear una  para editarla bici en la api correctamente"
                    );
                    var aBici2 =
                        '{ "code": 10, "color": "azul", "modelo": "playera", "lat": -80, "lng": -80 }';
                    request.post(
                        {
                            headers: headers,
                            url: base_url + "/update",
                            body: aBici2,
                        },
                        function (error, response, body) {
                            expect(response.statusCode).toBe(200);
                            var result = JSON.parse(body);
                            console.log(result);
                            expect(result.bici.color).toBe("azul");
                            expect(result.bici.modelo).toBe("playera");
                            expect(result.bici.ubicacion[0]).toBe(-80);
                            expect(result.bici.ubicacion[1]).toBe(-80);
                            done();
                            console.log(
                                "Editar la bici desde la api correctamente"
                            );
                        }
                    );
                }
            );
        });
    });
});
