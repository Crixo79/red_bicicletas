var express = require("express");
var router = express.Router();

router.get("/", function (req, res, next) {
	res.render("index", { title: "Primera Aplicacion de Express" });
});

module.exports = router;
