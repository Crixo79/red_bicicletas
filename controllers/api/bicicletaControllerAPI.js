var Bicicleta = require("../../models/bicicleta");
exports.bicicleta_list = function (req, res) {
	Bicicleta.find({}, function (err, bicicletas) {
		res.status(200).json({
			Bicicletas: bicicletas,
		});
	});
};
exports.bicicleta_create = function (req, res) {
	const bici = new Bicicleta({
		code: req.body.code,
		color: req.body.color,
		modelo: req.body.modelo,
		ubicacion: [req.body.lat, req.body.lng],
	});
	Bicicleta.add(bici, (err, bici) => {
		if (err) res.status(500);
		else
			res.status(200).json({
				bicicleta: bici,
			});
	});
};
exports.bicicleta_delete = function (req, res) {
	Bicicleta.removeByCode(req.body.code, (err, bici) => {
		if (err) res.status(500);
		else res.status(204).send();
	});
};
exports.bicicleta_update = function (req, res) {
	const bici = {
		_id: req.body._id,
		code: req.body.code,
		color: req.body.color,
		modelo: req.body.modelo,
		ubicacion: [req.body.lat, req.body.lng],
	};
	Bicicleta.findByIdAndUpdate(bici._id, bici, function (err, update) {
		if (err) res.status(500);
		else
			res.status(200).json({
				bici: bici,
			});
	});
};
