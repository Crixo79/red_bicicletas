var Bicicleta = require('../../models/bicicleta');
beforeEach(() => {Bicicleta.allBicis = [];}); //Se ejecuta previo a todos los tests
//beforeEach(() => {console.log('testeando...')});

describe('Bicicleta.allBicis', () => {
	it('Comienza vacia',()=>{
		expect(Bicicleta.allBicis.length).toBe(0);
	});
});
describe('Bicicleta.add', () => {
	it('Agregamos una bicicleta',()=>{
		expect(Bicicleta.allBicis.length).toBe(0);
		var a = new Bicicleta (1,"azul","urbana",[-31.436414, -64.152916]);
		Bicicleta.add(a);
		expect(Bicicleta.allBicis.length).toBe(1);
		expect(Bicicleta.allBicis[0]).toBe(a);
	});
});
describe('Bicicleta.findById', () => {
	it('Debe devolver la bici con id 1',()=>{
		expect(Bicicleta.allBicis.length).toBe(0);
		var aBici1 = new Bicicleta(1,"verde","urbana");
		var aBici2 = new Bicicleta(2,"rojo","playera");
		Bicicleta.add(aBici1);
		Bicicleta.add(aBici2);
		var targetBici1 = Bicicleta.findById(1);
		expect(targetBici1.id).toBe(1);
		expect(targetBici1.color).toBe(aBici1.color);
		expect(targetBici1.modelo).toBe(aBici1.modelo);
	});
});
describe('Bicicleta.allBicis', () => {
	it('Probando Remover una bici',()=>{
		expect(Bicicleta.allBicis.length).toBe(0);
		var aBici1 = new Bicicleta(1,"verde","urbana");
		Bicicleta.add(aBici1);
		var targetBici1 = Bicicleta.removeById(1);
		expect(Bicicleta.allBicis.length).toBe(0);
	});
});