var Bicicleta = require('../../models/bicicleta');
var request = require('request');
var server = require('../../bin/www');
beforeEach(() => {Bicicleta.allBicis = [];});
describe('Bicicleta API: ', () => {
	describe('GET BICICLETAS /', () => {
		it('Status 200', () => {
			var a = new Bicicleta (1,"azul","urbana",[-31.436414, -64.152916]);
			Bicicleta.add(a);
			request.get('http://localhost:3000/api/bicicletas', (error, response, body) => {
				expect(response.statusCode).toBe(200);
			});
		});
	});
	describe('POST BICICLETAS /create', () => {
		it('Status 200', (done) => {
			var headers = {'Content-Type': 'application/json'};
			var aBici = '{"id": 10, "color": "rojo", "modelo":"playera", "lat": -40, "lng": -40}';
			request.post({
				headers: headers,
				url: 'http://localhost:3000/api/bicicletas/create',
				body: aBici
			}, (error, response, body) => {
				expect(response.statusCode).toBe(200);
				expect(Bicicleta.findById(10).color).toBe("rojo");
				done();
			});
		});
	});
	describe('DELETE BICICLETAS /delete', () => {
		it('Status 204', () => {
			var a = new Bicicleta (10,"azul","urbana",[-31.436414, -64.152916]);
			Bicicleta.add(a);
			var aBici = '{"id": 10}';
			request.delete({
				url: 'http://localhost:3000/api/bicicletas/delete',
				body: aBici
			}, (error, response, body) => {
				expect(response.statusCode).toBe(204);				
			});		
		});
	});
	describe('POST BICICLETAS /update', () => {
		it('Status 200', (done) => {
			var a = new Bicicleta (1,"azul","urbana",[-31.436414, -64.152916]);
			Bicicleta.add(a);
			var headers = {'Content-Type': 'application/json'};
			var aBici = '{"id" : 1,"color" : "violeta","modelo" : "random","lat" : -31.435546, "lng" : -64.159041}';
			request.post({
				headers: headers,
				url: 'http://localhost:3000/api/bicicletas/update',
				body: aBici
			}, (error, response, body) => {
				expect(response.statusCode).toBe(200);
				expect(Bicicleta.findById(a.id).color).toBe("violeta");
				done();
			});
		});
	});
});
