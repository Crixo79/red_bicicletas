var mymap = L.map('main_map').setView([-31.436414, -64.152916], 13);
var marker = L.marker([-31.436414, -64.152916]).addTo(mymap);
L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: 'pk.eyJ1IjoibmlhY2VsbyIsImEiOiJja2hucHpwNmEwM3AyMnpwMG1lNWtnODJ3In0._ohSLyRgBpwp6yorm50ltQ'
}).addTo(mymap);

$.ajax({
	dataType: 'json',
	url: 'api/bicicletas',
	success: function(result) {
		console.log(result);
		result.bicicletas.forEach(function(bici){
			L.marker(bici.ubicacion, {title: bici.id}).addTo(mymap);
		});
	}
})